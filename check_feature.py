
import pandas as pd
import matplotlib.pyplot as plt

dataset = pd.read_excel(r'./0724_data/0724_high_risk/0724_low_and_high_Z_score.xlsx', header=0)

# 可视化模型参数和学习效果
plt.rcParams['font.sans-serif']=['SimHei']  #用来正常显示中文标签
plt.rcParams['axes.unicode_minus']=True   #用来正常显示负号

# #检查数据特征与Gleason的相关性
# (dataset.corr().loc['Gleason'].plot(kind='barh', figsize=(10,10)))
#
#
# # drop uncorrelated numeric features (threshold <0.2)
# corr = (dataset.corr().loc['综合指南与治疗的分类方法'])
# corr = corr[corr <= 0.1]
# cols_to_drop = corr.index.to_list()
# print(cols_to_drop)
# print(len(cols_to_drop))
# df = dataset.drop(cols_to_drop, axis=1)
#
# df.to_excel('./0724_data/0724_high_risk/0724_low_and_high_Z_score(corr13).xlsx', index=False)
#
# plt.savefig('./plt/综合指南与治疗的分类方法.jpg')
# plt.show()

import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler
from scipy.stats import ttest_ind, levene
from sklearn.linear_model import LassoCV
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import Lasso

import matplotlib.pyplot as plt #
from sklearn.metrics import roc_curve, roc_auc_score, classification_report
dataset = pd.read_excel(r'./0724_data/0724_high_risk/0724_low_and_high_Z_score(delete_Gleason).xlsx', header=0)

# 用来正常显示中文标签
plt.rcParams['font.sans-serif']=['SimHei']
# 用来正常显示负号
plt.rcParams['axes.unicode_minus']=False
# 解决中文显示问题-设置字体为黑体，保存为Microsoft YaHei格式可以显示指数部分的负号
plt.rcParams['font.sans-serif'] = ['Microsoft YaHei']



X = dataset.iloc [:,1:]
Y = dataset.iloc[:,0]
X_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.2, random_state=123,stratify=Y)


# alpha_lasso = 10 ** np.linspace(-3, 1, 100)
# lasso = Lasso()
# coefs_lasso = []
#
#
# for i in alpha_lasso:
#     lasso.set_params(alpha=i)
#     lasso.fit(X_train, y_train)
#     coefs_lasso.append(lasso.coef_)
# plt.figure(figsize=(12, 10))
# ax = plt.gca()
# ax.plot(alpha_lasso, coefs_lasso)
# ax.set_xscale('log')
# plt.axis('tight')
# plt.xlabel('alpha')
# plt.ylabel('weights: scaled coefficients')
# plt.title('Lasso regression coefficients Vs. alpha')
# plt.legend(dataset.drop('综合指南与治疗的分类方法', axis=1, inplace=False).columns)
# plt.savefig('./plt/alpha_choice.jpg')
# plt.show()

a=-3
lasso = Lasso(alpha=10**(a))
model_lasso = lasso.fit(X_train, y_train)
coef = pd.Series(model_lasso.coef_,index=X_train.columns)
print(coef[coef != 0].abs().sort_values(ascending = False))

fea = X_train.columns
a = pd.DataFrame()
a['feature'] = fea
a['importance'] = coef.values
a = a.sort_values('importance',ascending = False)
plt.figure(figsize=(12,8))
plt.barh(a['feature'],a['importance'])
plt.title('the importance features')
plt.savefig('./plt/lasso(1).png')
plt.show()


# # print("X_train:",X_train)
# # print("X_test:",X_test)
#
# MSEs = model_lassoCV.mse_path_
# """
# MSEs_mean, MSEs_std = [], []
# for i in range(len(MSEs)):
#     MSEs_mean.append(MSEs[i].mean())
#     MSEs_std.append(MSEs[i].std())
# """
# MSEs_mean = np.apply_along_axis(np.mean,1,MSEs)
# MSEs_std = np.apply_along_axis(np.std,1,MSEs)
#
# plt.figure()
# plt.errorbar(model_lassoCV.alphas_,MSEs_mean    #x, y数据，一一对应
#              , yerr=MSEs_std                    #y误差范围
#              , fmt="o"                          #数据点标记
#              , ms=3                             #数据点大小
#              , mfc="r"                          #数据点颜色
#              , mec="r"                          #数据点边缘颜色
#              , ecolor="lightblue"               #误差棒颜色
#              , elinewidth=2                     #误差棒线宽
#              , capsize=4                        #误差棒边界线长度
#              , capthick=1)                      #误差棒边界线厚度
# plt.semilogx()
# plt.axvline(model_lassoCV.alpha_,color = 'black',ls="--")
# plt.xlabel('Lambda')
# plt.ylabel('MSE')
# plt.show()
# coefs = model_lassoCV.path(X_train_raw,y_train,alphas = alphas, max_iter = 100000)[1].T
# plt.figure()
# plt.semilogx(model_lassoCV.alphas_,coefs, '-')
# plt.axvline(model_lassoCV.alpha_,color = 'black',ls="--")
# plt.xlabel('Lambda')
# plt.ylabel('Coefficients')
# plt.show()