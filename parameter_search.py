from xgboost import XGBClassifier
import pandas as pd
import matplotlib.pyplot as plt
import warnings
warnings.filterwarnings("ignore")
# 可视化模型参数和学习效果
plt.rcParams['font.sans-serif']=['SimHei']  #用来正常显示中文标签
plt.rcParams['axes.unicode_minus']=False   #用来正常显示负号
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV  # 参数调优

def GridSearch(X_train,y_train):
    base_model = XGBClassifier(random_state=123) # 默认 kernel='rbf'
    params = {'eta':[0.2,0.25],
            'learning_rate':[0.005,0.01,],
            'max_depth':[3,9],
            'min_child_weight':[1,7],
            'gamma':[0.2,0.3,0.4]}
    xgb = GridSearchCV(estimator=base_model, param_grid=params,cv=5)
    xgb.fit(X_train, y_train)
    best_params_ = xgb.best_params_
    return best_params_

# def rf_cv(n_estimators, min_samples_split, max_features, max_depth):
#     val = cross_val_score(
#         RandomForestClassifier(n_estimators=int(n_estimators),
#             min_samples_split=int(min_samples_split),
#             max_features=min(max_features, 0.999), # float
#             max_depth=int(max_depth),
#             random_state=2
#         ),
#         X, Y,scoring='roc_auc_ovr', cv=10
#     ).mean()
#     return val
#
# rf_bo = BayesianOptimization(
#     rf_cv,
#     {'n_estimators': (10, 250),
#     'min_samples_split': (2, 25),
#     'max_features': (0.1, 0.999),
#     'max_depth': (5, 15)}
# )
#
# rf_bo.maximize(
#         # init_points=5,  #执行随机搜索的步数
#         # n_iter=25,   #执行贝叶斯优化的步数
#     )
#
# print(rf_bo.max)


if __name__ == '__main__':
    dataset = pd.read_excel(r'./0724_high_risk/0724_low_and_high_Z_score.xlsx', header=0)
    X = dataset.iloc[:, 1:]
    Y = dataset.iloc[:, 0]
    X_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.2, random_state=123, stratify=Y)
    best_params_=GridSearch(X_train,y_train)
    print('best_params',best_params_)