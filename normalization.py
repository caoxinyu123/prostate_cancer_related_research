
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

import itertools




df = pd.read_excel('./add_new_sample/add_new_data_151.xls')
df = df.iloc[:,1:34]
# df = (df-df.mean())/df.std()
df = (df - df.min()) / (df.max() - df.min())
print(df)
df.to_excel('./add_new_sample/add_new_data_151_max_min.xlsx', index=False)