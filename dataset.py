from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.linear_model import LassoCV


plt.rcParams['font.sans-serif'] = ['SimHei']  # 解决中文显示问题-设置字体为黑体
plt.rcParams['axes.unicode_minus'] = False  # 解决保存图像是负号'-'显示为方块的问题


def read_dataset(path):
    data = pd.read_excel(path)
    return data



def boxplot(path):
    for i in range(1,43):
        df = pd.read_excel(path, usecols=[i])
        print(df)
        name = df.keys()[0]
        # print(name)
        img = sns.boxplot(y ='{}'.format(name), data=df,)
        # plt.savefig('./plt/{}.jpg'.format(name))
        plt.show()



def heatmap(path):
    df = read_dataset(path)
    sns.set(rc={'figure.figsize': (10, 10)})
    sns.heatmap(df.corr(),
                annot=True,
                linewidths=.2,
                center=0,
                cbar=False,
                cmap="PiYG")
    # plt.savefig('./plt/heatmap.jpg')
    plt.show()

def choies_feature(path, type, name):
    dataset = read_dataset(path)
    if type == 'corr':
        corr = (dataset.corr().loc[name])
        corr = corr[corr >= 0.1]
        cols_to_drop = corr.index.to_list()
        print(cols_to_drop)
        print(len(cols_to_drop))
        df = dataset.drop(cols_to_drop, axis=1)

        # df.to_excel('./0724_data/0724_high_risk/0724_low_and_high_Z_score(corr13).xlsx', index=False)
        #
        # plt.savefig('./plt/综合指南与治疗的分类方法.jpg')
        # plt.show()
    else:
        dataset = read_dataset(path)

        X = dataset.iloc [:,1:43]
        Y = dataset.iloc[:,0]
        X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.1, random_state=123,stratify=Y)

        alphas = np.logspace(-4,1,100)
        model_lassoCV = LassoCV(alphas = alphas, max_iter = 100000).fit(X_train,y_train)
        coef = pd.Series(model_lassoCV.coef_, index = X_train.columns)
        print(model_lassoCV.alpha_)
        print('%s %d'%('Lasso picked',sum(coef != 0)))
        index = coef[coef != 0].index
        print(index)




def normalization(path):
    df = read_dataset(path)
    df1 = df.iloc[:, 0]
    df = df.iloc[:, 1:43]

    # df = (df - df.min()) / (df.max() - df.min())
    df = (df - df.mean()) / (df.std())
    df = pd.concat([df1, df], axis=1)
    print(df)
    # df.to_excel('./0724_data/0724_high_risk/0724_low_and_high_Z_score（lasso11）.xlsx', index=False)


def dataset_split(type,path):
    data = read_dataset(path)
    X = data.iloc[:, 1:]
    y = data.iloc[:, 0]
    X, X_test, y, y_test = train_test_split(X, y, test_size=0.2, random_state=123, stratify=y)
    if type == 'test':
        return X, X_test, y, y_test
    elif type == 'train':
        X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.1, random_state=123, stratify=y)
        return X_train, X_val, y_train, y_val


def data_split(data,y):
    X = data
    y = y

    X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.1, random_state=123, stratify=y)
    return X_train, X_val, y_train, y_val


if __name__ == '__main__':
    path = r'./0724_data/0724_high_risk/0724_low_and_high_Z_score(lasso_8_delete_Gleason).xlsx'
    # boxplot(path)
    # heatmap(path)
    # type = 'lasso' # lasso
    # name = '综合指南与治疗的分类方法'
    # choies_feature(path, type, name)

    type1 = 'test'
    X_test, y_test = dataset_split(type1, path)
    print('X_test:\n',X_test)
    print('y_test:\n',y_test)
    type2 = 'train'
    X_train, X_val, y_train, y_val = dataset_split(type2,path)
    print('X_train:\n',X_train)
    print('X_val:\n', X_val)
    print('y_train:\n',y_train)
    print('y_val:\n',y_val)





