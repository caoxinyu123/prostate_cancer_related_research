from sklearn.model_selection import train_test_split
# from mlxtend.classifier import StackingCVClassifier
import pandas as pd
from sklearn.metrics import accuracy_score
from sklearn.tree import DecisionTreeClassifier
import pandas as pd
from sklearn.model_selection import cross_val_score
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier, VotingClassifier
from sklearn import svm
from xgboost import XGBClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.kernel_ridge import KernelRidge
from sklearn.linear_model import SGDClassifier
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import RadiusNeighborsClassifier
from sklearn.model_selection import GridSearchCV

from sklearn.metrics import precision_recall_curve, roc_auc_score, roc_curve
import numpy as np
import warnings
from dataset import read_dataset, dataset_split, data_split
from model import getModel

warnings.filterwarnings("ignore")
# from sklearn import datasets
# iris = datasets.load_iris()

def sigmoid(x):
    return 1.0 / (1 + np.exp(-x))

def relu(x):
    return np.maximum(0, x)


def tanh(x):
    return (np.exp(x) - np.exp(-x)) / (np.exp(x) + np.exp(-x))


def Ensemble_add_feature(train, test, target, clfs):
    # n_flods = 5
    # skf = list(StratifiedKFold(y, n_folds=n_flods))
    train_ = np.zeros((train.shape[0], len(clfs )))
    test_ = np.zeros((test.shape[0], len(clfs)))

    for j, clf in enumerate(clfs):
        '''依次训练各个单模型'''
        # print(j, clf)
        '''使用第1个部分作为预测，第2部分来训练模型，获得其预测的输出作为第2部分的新特征。'''
        # X_train, y_train, X_test, y_test = X[train], y[train], X[test], y[test]
        clf.fit(train, target)
        y_train = clf.predict(train)

        y_test = clf.predict(test)
        # print(y_test)
        ## 新特征生成
        # train_[:, j] = y_train ** 2
        # test_[:, j] = y_test ** 2

        # train_[:, j] = np.exp(y_train)
        # test_[:, j] = np.exp(y_test)

        # train_[:, j ] = sigmoid(y_train)
        # test_[:, j ] = sigmoid(y_test)

        # train_[:, j ] = tanh(y_train)
        # test_[:, j ] = tanh(y_test)

        train_[:, j] = relu(y_train)
        test_[:, j] = relu(y_test)

        # print(test_[:, 2 * j + 1])
        # print("val auc Score: %f" % r2_score(y_predict, dataset_d2[:, j]))
        print('Method ', j)

    train_ = pd.DataFrame(train_)
    test_ = pd.DataFrame(test_)
    # print(test_)
    return train_, test_

path = r'0724_data/0724_high_risk/0724_low_and_high_Z_score(lasso_8_delete_Gleason).xlsx'
type1 = 'train'
type2 = 'test'
dataset = read_dataset(path)
X, x_test, y, y_test = dataset_split(type2, path)
# 模型融合中使用到的各个单模型
clfs = [LogisticRegression(),
        RandomForestClassifier(random_state=123, n_estimators=100),
        DecisionTreeClassifier(random_state=123),
        KNeighborsClassifier(n_neighbors=5, p=2, metric='minkowski'),
        MLPClassifier(activation="relu", #['identity', 'logistic', 'relu', 'softmax', 'tanh']
         solver='adam', hidden_layer_sizes=(50,) ,alpha=0.001,
        learning_rate_init = 0.005,max_iter=50,momentum=0.9,random_state=123),
        # svm.SVC(C=5000,tol=1e-3,probability=True,degree=4,cache_size=50,random_state=123),
        XGBClassifier(reg_lambda=0.6, max_depth=6,random_state=123),
        # AdaBoostClassifier(n_estimators=50,learning_rate=0.01,random_state=123),
        LinearDiscriminantAnalysis(),


]


New_x, New_test = Ensemble_add_feature(X, x_test, y, clfs)

print(New_x.shape)


New_train, New_val, y_train, y_val = data_split(New_x,y)

def weight(model_acc):
    weights = []
    for i in range(len(model_acc)):
        weights.append(model_acc[i]/sum(model_acc))
    return weights

from sklearn.model_selection import KFold
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score, mean_squared_error, r2_score, confusion_matrix
import itertools
# 绘制混淆矩阵函数
def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    plt.figure()
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.show()


def K_fold(New_x,y,New_test,y_test):
    lr = getModel('lr')
    rf = getModel('rf')
    dt = getModel('dt')
    xgb = getModel('xgb')

    LDA = getModel('LDA')

    knn = getModel('knn')
    mlp = getModel('mlp')

    # clfs = [('forest', rf), ('lr', lr), ('xgb', xgb)]
    # clfs = [('forest', rf), ('lr', lr),('knn', knn),('LDA', LDA),('xgb', xgb)]
    clfs = [('forest', rf), ('lr', lr), ('knn', knn), ('LDA', LDA), ('xgb', xgb),('dt',dt),('mlp',mlp)]
    # clfs = [('lr', lr)]
    # clfs = [('rf', rf)]
    # clfs = [('dt', dt)]
    # clfs = [('LDA', LDA)]
    # clfs = [('xgb', xgb)]
    # clfs = [('knn', knn)]
    # clfs = [('mlp', mlp)]


    kf = KFold(n_splits=10)
    acc = []
    pro = []
    auc = []

    for train, val in kf.split(New_x,y):
        # print("%s\n %s\n" % (train, val))
        train_X = New_x.iloc[train]
        train_y = y.iloc[train]

        val_X = New_x.iloc[val]
        val_y = y.iloc[val]
        voting = VotingClassifier(estimators=clfs, voting='soft') # weights=[0.1,0.1,0.1,0.1,0.6]
        voting.fit(train_X,train_y)
        # 计算ACC
        y_test_pre = voting.predict(New_test)
        ACC = accuracy_score(y_test_pre,y_test)
        cnf_matrix = confusion_matrix(y_test, y_test_pre)
        print(cnf_matrix)
        class_names = [0, 1, 2]
        plot_confusion_matrix(cnf_matrix, classes=class_names, title='Confusion matrix')
        acc.append(ACC)
        # 计算AUC
        # y_test = pd.get_dummies(y_test)
        # y_test_pro = voting.predict_proba(New_test)
        # AUC = roc_auc_score(y_test, y_test_pro)
        # auc.append(AUC)



    # for i in range(len(pro)):
    #     y_test = pd.get_dummies(y_test)
    #     AUC = roc_auc_score(y_test, pro[i])
    #     auc.append(AUC)


    # print('acc:',sum(acc)/len(acc))
    # print('auc:',sum(auc) / len(auc))



# # 计算混淆矩阵
# cnf_matrix = confusion_matrix(y_test, y_test_pro)
# np.set_printoptions(precision=2)  # 设置打印数量的阈值
# class_names = [0, 1]
# # 绘制混淆矩阵
# plot_confusion_matrix(cnf_matrix, classes=class_names, title='Confusion matrix')

def train_ensemble(New_train, New_val, y_train, y_val,New_test, y_test):
    lr = getModel('lr')
    rf = getModel('rf')
    svm = getModel('svm')
    knn = getModel('knn')
    dt = getModel('dt')
    mlp = getModel('mlp')
    xgb = getModel('xgb')
    Ada = getModel('Ada')
    LDA = getModel('LDA')
    clfs = [('forest', rf),('lr',lr), ('dt',dt),('Ada',Ada),('xgb', xgb)]
    # clf = LogisticRegression()
    # clf = GradientBoostingClassifier(learning_rate=0.02, subsample=0.5, max_depth=6, n_estimators=30)
    # clf.fit(New_train, y_train)
    # y_pred1 = clf.predict(New_train)
    # print('训练验证准确率: %.4f' % accuracy_score(y_train, y_pred1))
    #
    # y_emb = clf.predict_proba(New_val)
    # # print('概率：', y_emb)
    #
    # y_pred2 = clf.predict(New_val)
    # print(New_val.shape)
    # print('验证准确率: %.4f' % accuracy_score(y_val, y_pred2))


    model_acc = []
    for clf in (lr, rf,dt,xgb,Ada):
        clf.fit(New_train, y_train)
        y_ = clf.predict(New_test)
        print(clf.__class__.__name__, accuracy_score(y_, y_test))
        model_acc.append(accuracy_score(y_,y_test))
        # print(clf.__class__.__name__, y_)
    # weights = weight(model_acc)
    # voting = VotingClassifier(estimators=clfs, weights=weights, voting='soft')
    # voting.fit(New_train, y_train)
    # y_v = voting.predict(New_test)
    # ACC = accuracy_score(y_v, y_test)
    #
    # y_test = pd.get_dummies(y_test)
    # y_prob = voting.predict_proba(New_test)
    # AUC = roc_auc_score(y_test, y_prob)
    #
    #
    # print(voting.__class__.__name__,ACC)
    # print(voting.__class__.__name__,AUC)





if __name__ == '__main__':
    # train_ensemble(New_train, New_val, y_train, y_val, New_test, y_test)
    K_fold(New_x,y,New_test,y_test)