
import matplotlib.pyplot as plt
plt.rcParams['font.sans-serif'] = ['SimHei']  # 解决中文显示问题-设置字体为黑体
plt.rcParams['axes.unicode_minus'] = False  # 解决保存图像是负号'-'显示为方块的问题
import pandas as pd
import matplotlib.pyplot as plt


# 对当前特征的属性进行统计
data = pd.read_excel('./0724_data/0724_high_risk/0724_low_and_high(delete_Gleason).xls')#,usecols= [9]

df = pd.DataFrame(data)
print(df.describe())


# 实现对42中特征箱线图的绘制
for i in range(1,43):
    print(i)
    data = pd.read_excel('./0724_data/0724_high_risk/0724_low_and_high.xls', usecols=[i])
    name = data.keys()[0]
    df = pd.DataFrame(data)
    # print(df.describe())
    df.plot.box(title="prostate")
    plt.grid(linestyle="--", alpha=0.3)
    plt.savefig('./plt/{}.jpg'.format(i))
    plt.show()

















