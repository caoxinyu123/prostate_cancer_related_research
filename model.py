# 分类器
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from xgboost import XGBClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.kernel_ridge import KernelRidge
from sklearn.linear_model import SGDClassifier
from sklearn.gaussian_process import GaussianProcessClassifier


classifiers_ = {
    'lr': LogisticRegression(),
    'rf': RandomForestClassifier(random_state=1, n_estimators=100),
    'svm': SVC(C=5000,tol=1e-3,probability=True,degree=4,cache_size=50,random_state=123),
    'knn': KNeighborsClassifier(n_neighbors=5, p=2, metric='minkowski'),
    'dt': DecisionTreeClassifier(random_state=123),
    'mlp': MLPClassifier(activation="relu", #['identity', 'logistic', 'relu', 'softmax', 'tanh']
         solver='adam', hidden_layer_sizes=(50,) ,alpha=0.001,
        learning_rate_init = 0.005,max_iter=50,momentum=0.9,
            random_state=123),
    'xgb': XGBClassifier(reg_lambda=0.6, max_depth=6,random_state=123),
    'Ada': AdaBoostClassifier(n_estimators=50,learning_rate=0.01,random_state=0),
    'LDA': LinearDiscriminantAnalysis(),
    'SGD': SGDClassifier(loss="hinge", penalty="l2", max_iter=5,random_state=123),
    'GP': GaussianProcessClassifier(random_state=123),

}

def getModel(name):
    return classifiers_[name]

