import matplotlib.pylab as plt
from matplotlib import ticker


major = 10
minor = 2.5
fig = plt.figure(figsize=(8, 8))


def plt_nomogram(rows, column, current, starting, ending, major, minor, name, site=[]):
    ax = plt.subplot(rows, column, current)
    ax.yaxis.set_major_locator(ticker.NullLocator())
    ax.spines['right'].set_color('none')
    ax.spines['left'].set_color('none')
    ax.spines['bottom'].set_color('none')
    ax.xaxis.set_major_locator(ticker.MultipleLocator(major))
    ax.xaxis.set_minor_locator(ticker.MultipleLocator(minor))
    ax.xaxis.set_ticks_position("top")
    ax.set_xlim(starting, ending)
    ax.set_ylabel("{}".format(name), loc='bottom', fontsize=9, fontname='Monospace', color='black', rotation=0, labelpad=67)
    ax.set_position(site)



rows = 9
column = 1
X0 = 0.15
# 设置y的标签与x轴的间距
labelpad = 40
# 修改site[x,y,w,h] x:subplot与当前左边间距 y：subplot与当前上边间距 w:subplot的宽 h：subplot的高
plt_nomogram(rows, column, 1, 0, 100, 10, 2.5, "points", [X0,0.9,0.8,0.001])
plt_nomogram(rows, column, 2, 25, 75, 25, 5, "1", [X0,0.8,0.064,0.001])
plt_nomogram(rows, column, 3, 0, 1000, 200, 50, "2", [X0,0.7,0.336,0.001])
plt_nomogram(rows, column, 4, 150, 600, 150, 75, "3", [X0,0.6,0.12,0.001])
plt_nomogram(rows, column, 5, 0, 100, 50, 25, "4", [X0,0.5,0.064,0.001])
plt_nomogram(rows, column, 6, 0, 90, 30, 10, "5", [X0,0.4,0.104,0.001])
plt_nomogram(rows, column, 7, 0, 10, 5, 2.5, "6", [X0,0.3,0.112,0.001])
plt_nomogram(rows, column, 8, 0, 100, 10, 2.5, "total points", [X0,0.2,0.8,0.001])
plt_nomogram(rows, column, 9, 0, 0.9, 0.1, 0.25, "prob", [X0,0.1,0.69,0.001])


# fig = plt.figure(figsize=(8,8))
# ax = plt.subplot(411)
# ax.yaxis.set_major_locator(ticker.NullLocator())
# ax.spines['right'].set_color('none')
# ax.spines['left'].set_color('none')
# ax.spines['bottom'].set_color('none')
# ax.xaxis.set_major_locator(ticker.MultipleLocator(major))
# ax.xaxis.set_minor_locator(ticker.MultipleLocator(minor))
# ax.xaxis.set_ticks_position("top")
# ax.set_xlim(0, 100)
# ax.set_ylabel("point",loc='bottom',fontsize=9, fontname='Monospace', color='black')
# ax.set_position([0.1,0.8,0.8,0.001])
#
# ax = plt.subplot(412)
# ax.yaxis.set_major_locator(ticker.NullLocator())
# ax.xaxis.set_major_locator(ticker.MultipleLocator(major))
# ax.xaxis.set_minor_locator(ticker.MultipleLocator(minor))
# ax.xaxis.set_ticks_position("top")
# ax.spines['right'].set_color('none')
# ax.spines['left'].set_color('none')
# ax.spines['bottom'].set_color('none')
# ax.set_xlim(0, 85)
# ax.set_ylabel("age",loc='bottom',fontsize=9, fontname='Monospace', color='black')
# ax.set_position([0.2,0.6,0.4,0.001])
#
# ax = plt.subplot(413)
# ax.yaxis.set_major_locator(ticker.NullLocator())
# ax.xaxis.set_major_locator(ticker.MultipleLocator(major))
# ax.xaxis.set_minor_locator(ticker.MultipleLocator(minor))
# ax.xaxis.set_ticks_position("top")
# ax.spines['right'].set_color('none')
# ax.spines['left'].set_color('none')
# ax.spines['bottom'].set_color('none')
#
# ax.set_xlim(35, 85)
# ax.set_ylabel("PSA",loc='bottom',fontsize=9, fontname='Monospace', color='black')
# ax.set_position([0.2,0.4,0.4,0.001])



fig.savefig("./plt/tmp.png",dpi=300)

