import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
import warnings
from sklearn.linear_model import LassoCV

# from dataset import read_dataset,dataset_split

warnings.filterwarnings('ignore')
plt.rcParams['font.sans-serif'] = ['Microsoft YaHei']
# 解决中文显示问题-设置字体为黑体，保存为Microsoft YaHei格式可以显示指数部分的负号
plt.rcParams['axes.unicode_minus'] = False
# 解决保存图像是负号'-'显示为方块的问题
def Lasso_alpha(min,max,interval,max_iter):


    alphas = np.logspace(min,max,interval)
    model_lassoCV = LassoCV(alphas = alphas, max_iter=max_iter ).fit(X_train,y_train)
    coefs = model_lassoCV.path(X_train,y_train,alphas = alphas, max_iter=max_iter)[1].T
    alphas = model_lassoCV.alpha_

    # 画图
    # plt.figure(figsize=(12, 12))
    # plt.semilogx(model_lassoCV.alphas_,coefs, '-')
    # plt.axvline(model_lassoCV.alpha_,color = 'black',ls="--")
    # plt.xlabel('Lambda')
    # plt.ylabel('Coefficients')
    # plt.legend(dataset.drop('综合指南与治疗的分类方法', axis=1, inplace=False).columns, loc='lower right')
    # plt.show()

    coef = pd.Series(model_lassoCV.coef_, index = X_train.columns)
    coef_value=coef[coef != 0].abs().sort_values(ascending = False)
    fea = coef[coef != 0].index
    # 画图
    a = pd.DataFrame()
    a['feature'] = fea
    a['importance'] = coef[coef != 0].values
    a = a.sort_values('importance', ascending=False)
    plt.figure(figsize=(12, 6))
    plt.barh(a['feature'], a['importance'])
    plt.title('the importance features')
    plt.show()
    num=sum(coef != 0)
    index = coef[coef != 0].index
    print(index)



    return coef_value,alphas,num,index
if __name__ == '__main__':
    dataset = pd.read_excel(r'./0724_data/0724_high_risk/0724_low_and_high_Z_score(delete_Gleason).xlsx', header=0)
    X = dataset.iloc[:, 1:]
    Y = dataset.iloc[:, 0]
    X_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.2, random_state=123, stratify=Y)

    coef_value, alpha, num, index = Lasso_alpha(-4,3,100,100000)
    print(dataset.shape,dataset[index].shape)
    # New_data = dataset.merge(Y,dataset[index],left_on='综合指南与治疗的分类方法',right_on='年龄',how='inner')
    New_data = pd.concat([Y,dataset[index]],axis=1)
    print(New_data)


    # print(coef_value)
    # print(coef)
    # print('alpha:',alpha)
    # print(num)